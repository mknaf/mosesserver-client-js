$("#gobutton").on("click", function() {
    $.xmlrpc({
        url: "/RPC2",
        methodName: "translate",
        params: [{
            "text": $("#srctext").text(),
            "align": "false",
            "report-all-factors": "false"
        }],
        beforeSend: function() {
            $("#gobutton").addClass("loading");
            $("#trgtext").text("");
        },
        success: function(response, status, jqXHR) {
            $("#trgtext").html(response[0]["text"]);
            $("#gobutton").removeClass("loading");
        },
        error: function(jqXHR, status, error) {
            $("#trgtext").text("An error occured: " + error);
            $("#gobutton").removeClass("loading");
        }
    });
});
