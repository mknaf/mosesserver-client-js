# Makefile for minifying minifying webnlp frontend

all:

debug:
	rm -rf dist
	ln -s src dist

release:
	# copy complete src dir over
	rm -rf dist
	mkdir -p dist
	cp -rf src/* dist/
	#
	# if yui-compressor is not installed, this will fail and there
	# will simply be no minified versions. the site will then work
	# as if in debug mode.
	#
	for file in $(shell find src/js/lingenio*js); do \
		yui-compressor $$file -o dist/js/`basename $$file`; \
		done
