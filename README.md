# About
A simple example for a JavaScript frontend to `mosesserver`.

This is here mainly for reference in case we ever want to implement a direct
access to mosesserver without any middleware, for example for a customer.

Normally (for example in wwwtranslate), we use Lingenio's Python  `middleware`
to access a `mosesserver`.